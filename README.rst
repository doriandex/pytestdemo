Pytest Demo für beispielhafte Nutzung des Gitlab CI/CD Tools - Automatisch Testen in Gitlab Workshop Hamburg 02/2020
--------------------------------------------------------------------------------------------------------------------

Tests:
======

def test_addition():
    summe = addition(1, 5)
    assert summe == 6


def test_addition_will_fail():
    summe = addition(1, 5)
    assert summe == 7

Screenshots
-----------

.. image:: images/pytest.PNG

