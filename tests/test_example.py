from ..example import addition


def test_addition():
    summe = addition(1, 7)
    assert summe == 8


def test_addition_will_fail():
    summe = addition(1, 5)
    assert summe == 9
